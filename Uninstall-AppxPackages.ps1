# This script will uninstall all appxpackages listed
# use get-appxpackage | select name to get all appxpackages name
$logFile = "C:\ProgramData\F5_CTG\Uninstall-AppxPackages.txt"
Start-Transcript $logFile
$AppsList = @(
	"Microsoft.Getstarted" # Get Started
	"Microsoft.YourPhone" # Your Phone
	"Microsoft.MicrosoftSolitaireCollection" # Microsoft Solitaire Collection
	"Microsoft.ZuneVideo" #Movies & TV
	"Microsoft.ZuneMusic" # Music
	"Microsoft.Xbox.TCUI"
	"Microsoft.XboxGameOverlay"
	"Microsoft.XboxGamingOverlay"
	"Microsoft.XboxIdentityProvider"
	"Microsoft.XboxSpeechToTextOverlay"
	"Microsoft.MicrosoftOfficeHub" # Office hub
	"Microsoft.GamingApp"
	"Microsoft.WindowsFeedbackHub"
)
ForEach ($App in $AppsList) {
	$Packages = Get-AppxPackage | Where-Object { $_.Name -eq $App }
	if ($null -ne $Packages) {
		"Removing Appx Package: $App"
		foreach ($Package in $Packages) { Remove-AppxPackage -package $Package.PackageFullName }
	}
	else { "Unable to find package: $App" }

	$ProvisionedPackage = Get-AppxProvisionedPackage -online | Where-Object { $_.displayName -eq $App }
	if ($null -ne $ProvisionedPackage) {
		"Removing Appx Provisioned Package: $App"
		Remove-AppxProvisionedPackage -online -packagename $ProvisionedPackage.PackageName -AllUsers
	}
	else { "Unable to find provisioned package: $App" }
}
Stop-Transcript